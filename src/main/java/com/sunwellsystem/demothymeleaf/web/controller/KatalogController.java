/*
 * KatalogController.java
 *
 * Created on Feb 12, 2021, 14.49
 */
package com.sunwellsystem.demothymeleaf.web.controller;

import com.sunwellsystem.demothymeleaf.domain.entity.Buku;
import com.sunwellsystem.demothymeleaf.domain.service.KatalogService;
import org.springframework.stereotype.Controller;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author irfin
 */
@Controller
public class KatalogController
{
    @Autowired
    private KatalogService service;

    public KatalogController()
    {
        super();
        System.out.println("CTOR KatalogController() called.");
    }

//    @ModelAttribute("allBuku")
//    public List<Buku> populateAllBuku()
//    {
//        Iterable<Buku> allBuku = service.getAllBuku();
//        LinkedList<Buku> retval = new LinkedList();
//        for (Buku buku : allBuku)
//            retval.add(buku);
//
//        return retval;
//    }

    @GetMapping("/index")
    public String showBookList(Model model)
    {
        Iterable<Buku> allBuku = service.getAllBuku();
        for (Buku buku : allBuku) {
            System.out.println(buku.toString());
        }

        model.addAttribute("books", allBuku);
        return "index";
    }

    @GetMapping("/addbook")
    public String showAddBookForm(Model model)
    {
        model.addAttribute("newBook", new Buku());
        return "add-buku";
    }

    @PostMapping("/addbook")
    public String addBook(final Buku bookEntity, BindingResult bindingResult, Model model)
    {
        if (bindingResult.hasErrors()) {
            return "add-buku";
        }

        service.create(bookEntity);

        return "redirect:/index";
    }


//    @RequestMapping({"/","/seedstartermng"})
//    public String showBuku(final Buku buku) {
//        return "seedstartermng";
//    }
//
//    @RequestMapping(value="/seedstartermng", params={"save"})
//    public String saveBuku(final Buku buku, final BindingResult bindingResult, final ModelMap model)
//    {
//        if (bindingResult.hasErrors()) {
//            return "seedstartermng";
//        }
//        service.create(buku);
//        model.clear();
//
//        return "redirect:/seedstartermng";
//    }

//    @RequestMapping(value="/seedstartermng", params={"addRow"})
//    public String addRow(final SeedStarter seedStarter, final BindingResult bindingResult) {
//        seedStarter.getRows().add(new Row());
//        return "seedstartermng";
//    }
//
//
//    @RequestMapping(value="/seedstartermng", params={"removeRow"})
//    public String removeRow(final SeedStarter seedStarter, final BindingResult bindingResult, final HttpServletRequest req) {
//        final Integer rowId = Integer.valueOf(req.getParameter("removeRow"));
//        seedStarter.getRows().remove(rowId.intValue());
//        return "seedstartermng";
//    }
}
