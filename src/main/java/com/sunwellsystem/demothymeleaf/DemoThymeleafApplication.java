package com.sunwellsystem.demothymeleaf;

import com.sunwellsystem.demothymeleaf.web.controller.KatalogController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoThymeleafApplication implements CommandLineRunner
{
	public static void main(String[] args)
	{
		SpringApplication.run(DemoThymeleafApplication.class, args);
	}

//	@Autowired
//	private KatalogController katalogController;

	@Override
	public void run(String... args) throws Exception
	{
//		System.out.println("@@@@@@@@@@@@@");
//		System.out.println("MULAI TESTING");
//		System.out.println("@@@@@@@@@@@@@");
//
//		katalogController.showBookList(null);
	}
}
