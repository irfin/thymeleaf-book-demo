/*
 * KatalogService.java
 *
 * Created on Feb 12, 2021, 14.50
 */
package com.sunwellsystem.demothymeleaf.domain.service;

import com.sunwellsystem.demothymeleaf.domain.entity.Buku;
import com.sunwellsystem.demothymeleaf.persistent.repository.BukuRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author irfin
 */
@Service
public class KatalogService
{
    @Autowired
    private BukuRepo bukuRepo;

    public Iterable<Buku> getAllBukuSortByKategori()
    {
        return bukuRepo.findAllBukuSortByKategori();
    }

    public Iterable<Buku> getAllBuku()
    {
        return bukuRepo.findAll();
    }

    public Optional<Buku> getByIsbn(String isbn)
    {
        return bukuRepo.findById(isbn);
    }

    public void create(Buku b)
    {
        bukuRepo.save(b);
    }
}
