/*
 * KategoriBuku.java
 *
 * Created on Feb 12, 2021, 14.23
 */
package com.sunwellsystem.demothymeleaf.domain.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @author irfin
 */
@Entity
@Table(name = "kategoribuku")
@Data
public class KategoriBuku
{
    @Id
    private String kode;

    @Column
    private String nama;
}
