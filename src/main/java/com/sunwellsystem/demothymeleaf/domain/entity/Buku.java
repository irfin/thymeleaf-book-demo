/*
 * Buku.java
 *
 * Created on Feb 12, 2021, 14.23
 */
package com.sunwellsystem.demothymeleaf.domain.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author irfin
 */
@Data
@Entity
public class Buku
{
    @Id
    private String isbn;

    @Column
    private String judul;

    @Column(name = "tahunterbit")
    private int tahunTerbit;

    @ManyToOne
    @JoinColumn(name = "kategoribuku_id")
    private KategoriBuku kategori;
}
