/*
 * BukuRepo.java
 *
 * Created on Feb 12, 2021, 14.51
 */
package com.sunwellsystem.demothymeleaf.persistent.repository;

import com.sunwellsystem.demothymeleaf.domain.entity.Buku;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author irfin
 */
public interface BukuRepo extends JpaRepository<Buku, String>
{
    @Query("SELECT b FROM Buku b ORDER BY b.kategori.nama")
    public Iterable<Buku> findAllBukuSortByKategori();
}
